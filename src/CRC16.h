/*
 * CRC16.h
 *
 *  Created on: 08.01.2018
 *      Author: thomas
 */
#include "Arduino.h"

#ifndef CRC16_H_
#define CRC16_H_


// class to calculate crc16 as specified by DIN EN 62056-46
class CRC16 {
private:
	unsigned int crc;
	unsigned int temp;

public:
	unsigned int add(unsigned char data);
	unsigned int getCrc();
	void reset();
	CRC16();
};

#endif /* CRC16_H_ */
